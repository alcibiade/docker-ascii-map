#!/usr/bin/env python3
import sys
import time

from docker_ascii_map.argument_parser import get_input_parameters
from docker_ascii_map.ascii_render import Renderer
from docker_ascii_map.debug import dump_configuration
from docker_ascii_map.docker_config import ConfigParser


def follow(color_mode):
    try:
        config_parser = ConfigParser()

        while True:
            print(chr(27) + "[2J")
            print(chr(27) + "[;H")
            renderer = Renderer()
            config = config_parser.get_configuration()
            text = renderer.render(config, encoding=sys.stdout.encoding, color=color_mode)
            print(text)
            time.sleep(2)
    except KeyboardInterrupt as _:
        pass


def main():
    color_mode, follow_mode, debug_mode = get_input_parameters()

    if follow_mode:
        follow(color_mode)
    else:
        config_parser = ConfigParser()
        renderer = Renderer()
        config = config_parser.get_configuration()

        if debug_mode:
            print('===== Configuration ========================================')
            print(dump_configuration(config))
            print('============================================================')

        text = renderer.render(config, encoding=sys.stdout.encoding, color=color_mode)
        print(text)


if __name__ == '__main__':
    main()
