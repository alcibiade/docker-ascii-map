import unittest

from docker_ascii_map.debug import restore_configuration, dump_configuration
from docker_ascii_map.docker_config import Configuration, Container


class DumpTest(unittest.TestCase):
    def test_dump_and_restore(self):
        config = Configuration([
            Container('n1', 'running', ['net1'], 'group1/image-long-name'),
            Container('n2', 'stopped', ['net2'], 'group2/image-short'),
        ])

        s = dump_configuration(config)
        config_clone = restore_configuration(s)
        self.assertEqual(config, config_clone)


if __name__ == '__main__':
    unittest.main()
