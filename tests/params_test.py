import sys
import unittest
from argparse import ArgumentParser
from unittest.mock import patch

import docker_ascii_map.argument_parser


class Args:
    def __init__(self, mono=None, color=None, follow=None, debug=None):
        self.mono = mono
        self.color = color
        self.follow = follow
        self.debug = debug


class ParametersTests(unittest.TestCase):
    def test_monoterm(self):
        with patch('sys.platform', return_value='linux') as mock_getenv, \
            patch.object(sys.stdout, 'isatty', return_value=False) as mock_stdout, \
            patch.object(ArgumentParser, 'parse_args', return_value=Args()) as mock_parse:
            r = docker_ascii_map.argument_parser.get_input_parameters()
            self.assertEqual(False, r[0])

    def test_colorterm(self):
        with patch('sys.platform', return_value='linux') as mock_getenv, \
            patch.object(sys.stdout, 'isatty', return_value=True) as mock_stdout, \
            patch.object(ArgumentParser, 'parse_args', return_value=Args()) as mock_parse:
            r = docker_ascii_map.argument_parser.get_input_parameters()
            self.assertEqual(True, r[0])

    def test_colorterm_win(self):
        with patch('sys.platform', return_value='win32') as mock_getenv, \
            patch.object(sys.stdout, 'isatty', return_value=True) as mock_stdout, \
            patch('os.environ', return_value=['ANSICON']) as mock_environ, \
            patch.object(ArgumentParser, 'parse_args', return_value=Args()) as mock_parse:
            r = docker_ascii_map.argument_parser.get_input_parameters()
            self.assertEqual(True, r[0])

    def test_follow_default(self):
        with patch('sys.platform', return_value='linux') as mock_getenv, \
            patch.object(sys.stdout, 'isatty', return_value=True) as mock_stdout, \
            patch.object(ArgumentParser, 'parse_args', return_value=Args(follow=None)) as mock_parse:
            r = docker_ascii_map.argument_parser.get_input_parameters()
            self.assertEqual((True, False, False), r)

    def test_follow_on(self):
        with patch('sys.platform', return_value='linux') as mock_getenv, \
            patch.object(sys.stdout, 'isatty', return_value=True) as mock_stdout, \
            patch.object(ArgumentParser, 'parse_args', return_value=Args(follow='follow')) as mock_parse:
            r = docker_ascii_map.argument_parser.get_input_parameters()
            self.assertEqual((True, True, False), r)

    def test_debug_on(self):
        with patch.object(ArgumentParser, 'parse_args', return_value=Args(debug='debug')) as mock_parse:
            r = docker_ascii_map.argument_parser.get_input_parameters()
            self.assertEqual((False, False, True), r)


if __name__ == '__main__':
    unittest.main()
