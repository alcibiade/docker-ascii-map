import unittest
from typing import List
from unittest.mock import patch

import docker

from docker_ascii_map.docker_config import ConfigParser, PortMapping


class APIClientStub:
    def __init__(self, containers: List):
        self._containers = containers

    def containers(self, **kwargs):
        return self._containers


class ConfigTests(unittest.TestCase):
    def test_portmapping(self):
        self.assertEqual('80:8080', str(PortMapping(private_port=8080, public_port=80)))

    def test_empty_config(self):
        with patch.object(docker, 'APIClient', return_value=APIClientStub([])):
            configuration_parser = ConfigParser()
            self.assertEqual([], configuration_parser.get_configuration().containers)

    def test_single_container(self):
        with patch.object(docker, 'APIClient', return_value=APIClientStub([
            {'Names': ['/im1'], 'State': 'running', 'Image': 'ubuntu:latest',
             'Mounts': [],
             'NetworkSettings': {'Networks': {}}, 'Ports': []}])):
            configuration_parser = ConfigParser()
        self.assertEqual('[\'im1 - running - [] [] []\']', str(configuration_parser.get_configuration()))

    def test_volume(self):
        with patch.object(docker, 'APIClient', return_value=APIClientStub([
            {'Names': ['/im1'], 'State': 'running', 'Image': 'ubuntu:latest',
             'Mounts': [{'Source': '/home/yk/Documents/hr/pg-data', 'RW': True, 'Mode': '',
                         'Type': 'bind', 'Destination': '/var/lib/postgresql/data', 'Propagation': ''}],
             'NetworkSettings': {'Networks': {}}, 'Ports': []}])):
            configuration_parser = ConfigParser()
            self.assertEqual('[\'im1 - running - [] [] [/home/yk/Documents/hr/pg-data:/var/lib/postgresql/data]\']',
                             str(configuration_parser.get_configuration()))
